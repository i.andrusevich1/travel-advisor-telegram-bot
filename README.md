# Travel Advisor Telegram Bot

This project includes back-end source code of a web-application that provides both:
*  integration with a Telegram bot that can give travel advises about cities inquered by users
*  REST services to perform CRUD operations with the cities


## Run options
You can run and test the app in following ways:
*  by cloning the source code and running the app on your machine, which also makes possible to open the Telegram bot in Telegram (see its credentials below) 
*  by finding the working Telegram bot using its credentials below without running the app by yoursef (please, concact the author to make sure that the app is running)

 
## Credentials  



| Name | Username | Token | 
| ------ | ------ | ------ | 
| Traveler Advisor | traveler_advisor_bot | 1141445512:AAF0WatoeUIonsztYfZMaIqTwGxWqjAMfZE | 

## Running the app

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

To install and run the application you may need:
* [JDK 11+](https://jdk.java.net/11/) - JDK
* [Maven](https://maven.apache.org/) - Builder
* [PostgreSQL 9.+ ](https://www.postgresql.org/download/) - Database
* [Postman](https://www.getpostman.com/downloads/) - API executor

### Installing


1.  Download or clone the project
2.  Create a new schema in your database
3.  Run the the following sql-scripts in your database:
    *  src/main/resources/ddl.sql
    *  src/main/resources/dml.sql
4.  Specify your database parameters in the following file:
    *  src/main/resources/application.properties 


```
Most likely you will need to change only password to your database

spring.datasource.url=jdbc:postgresql://localhost:5432/travel_adviser
spring.datasource.username = postgres
spring.datasource.password  = *YOUR_PASSWORD_HERE*
```
5. Build the project with Maven 
```
Go to the project directory in command line and use command 
>  mvn spring-boot:run
```

6. Go to Telegram and type the name of the bot. Open dialog window, click "Start" and enjoy using the bot

7. To access REST API of the app use Postman


## Running the tests

Maven runs all the unit and integration tests automatically when building the project but you can run the tests independently:
```
Go to the project directory in command line and use command 
>  mvn test
```
