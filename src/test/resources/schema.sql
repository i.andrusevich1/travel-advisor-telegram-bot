create table city (id bigserial, name varchar(150), description varchar);

create sequence if not exists city_id_sequence increment by 10;