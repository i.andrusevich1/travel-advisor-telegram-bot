package com.andrusevich.pets.traveladvisor.service;

import com.andrusevich.pets.traveladvisor.persistence.entity.City;
import com.andrusevich.pets.traveladvisor.persistence.repository.CityRepository;
import com.andrusevich.pets.traveladvisor.service.dto.CityDto;
import com.andrusevich.pets.traveladvisor.service.mapper.CityMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Only a few service methods covered with tests just to verify that the tests generally work
 */
@RunWith(SpringRunner.class)
public class CityServiceTest {

    @Mock
    private CityRepository repository;

    @Mock
    private CityMapper mapper;

    @InjectMocks
    private CityService service;

    private List<City> cities;

    private List<CityDto> dtos;

    @Before
    public void setUp(){
        City city1 = City.builder().id(1).name("город1").description("desc1").build();
        City city2 = City.builder().id(2).name("город2").description("desc2").build();
        cities = List.of(city1, city2);

        CityDto dto1 = CityDto.builder().id(1).name("город1").description("desc1").build();
        CityDto dto2 = CityDto.builder().id(2).name("город2").description("desc2").build();
        dtos = List.of(dto1, dto2);
    }

    @Test
    public void getAllTest(){
        //given
        when(repository.findAll()).thenReturn(cities);
        when(mapper.toDto(cities.get(0))).thenReturn(dtos.get(0));
        when(mapper.toDto(cities.get(1))).thenReturn(dtos.get(1));

        //when
        List<CityDto> actual = service.getAll();

        //then
        verify(repository).findAll();
        verify(mapper).toDto(cities.get(0));
        verify(mapper).toDto(cities.get(1));
        Assert.assertEquals(dtos, actual);
    }

    @Test
    public void testCreate(){
        //given
        when(repository.save(cities.get(0))).thenReturn(cities.get(0));
        when(mapper.toDto(cities.get(0))).thenReturn(dtos.get(0));
        when(mapper.toEntity(dtos.get(0))).thenReturn(cities.get(0));

        //when
        CityDto actual = service.create(dtos.get(0));

        //then
        verify(repository).save(cities.get(0));
        verify(mapper).toDto(cities.get(0));
        verify(mapper).toEntity(dtos.get(0));
        Assert.assertEquals(dtos.get(0), actual);
    }
}
