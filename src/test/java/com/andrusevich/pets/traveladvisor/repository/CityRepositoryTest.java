package com.andrusevich.pets.traveladvisor.repository;

import com.andrusevich.pets.traveladvisor.persistence.entity.City;
import com.andrusevich.pets.traveladvisor.persistence.repository.CityRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Only a few repository methods covered with tests just to verify that the tests generally work
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
public class CityRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CityRepository repository;

    @Before
    public void setUo() {
        entityManager.clear();
    }

    @Test
    public void testFindByName() {
        //given
        City expected = City.builder().id(1).name("Москва").description("Описание Москвы").build();

        //when
        City actual = repository.findByName("Москва");

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testUpdate() {
        //given
        City expected = City.builder().id(1).name("Тест").description("Описание").build();

        //when
        City temp = repository.save(expected);
        entityManager.flush();
        repository.findById(temp.getId()).get();

        //then
        Assert.assertEquals(expected, temp);
    }
}
