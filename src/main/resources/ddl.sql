create database travel_adviser with owner postgres;

create table city (id bigserial, name varchar(150), description text);

create sequence if not exists city_id_sequence increment by 10;
SELECT setval('city_id_sequence', max(id)+1) FROM city;
