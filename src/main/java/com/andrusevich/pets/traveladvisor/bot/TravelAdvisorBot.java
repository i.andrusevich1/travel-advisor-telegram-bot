package com.andrusevich.pets.traveladvisor.bot;

import com.andrusevich.pets.traveladvisor.service.CityService;
import com.andrusevich.pets.traveladvisor.service.dto.CityDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
@PropertySource("classpath:telegram.properties")
public class TravelAdvisorBot extends TelegramLongPollingBot {
    private static final Logger LOGGER = LoggerFactory.getLogger(TravelAdvisorBot.class);
    private static final String START_COMMAND = "/start";

    @Value("${token}")
    private String token;

    @Value("${name}")
    private String name;

    @Autowired
    private CityService service;

    @Override
    public void onUpdateReceived(Update update) {
        String input = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();
        SendMessage response = new SendMessage();
        response.setChatId(chatId);

        if (START_COMMAND.equals(input)) {
            response.setText("Добро пожаловать!");
        } else if (service.findByName(input) == null) {
            response.setText("Извините, мы пока ничего не знаем об этом городе. \n" +
                    "Мы отправимся туда в ближайший отпуск!");
        } else {
            response.setText(service.findByName(input).getDescription());
        }

        SendMessage responseWithOptions = new SendMessage()
                .setChatId(chatId)
                .setText("Выберите город, о котором вы хотели бы узнать побольше:")
                .setReplyMarkup(createKeyBoard());

        try {
            execute(response);
            execute(responseWithOptions);
        } catch (TelegramApiException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public String getBotUsername() {
        return name;
    }

    @Override
    public String getBotToken() {
        return token;
    }

    private ReplyKeyboard createKeyBoard() {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> rows = new ArrayList<>();
        for (CityDto city : service.getAll()) {
            KeyboardRow row = new KeyboardRow();
            row.add(city.getName());
            rows.add(row);
        }
        keyboardMarkup.setKeyboard(rows);
        return keyboardMarkup;
    }
}
