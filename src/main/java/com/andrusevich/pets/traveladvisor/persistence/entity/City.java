package com.andrusevich.pets.traveladvisor.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "city")
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cityIdGenerator")
    @SequenceGenerator(name = "cityIdGenerator", sequenceName = "city_id_sequence", allocationSize = 10)
    @Column
    private long id;

    @Column
    private String name;

    @Column
    private String description;
}
