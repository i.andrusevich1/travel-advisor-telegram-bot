package com.andrusevich.pets.traveladvisor.persistence.repository;

import com.andrusevich.pets.traveladvisor.persistence.entity.City;
import org.springframework.data.repository.CrudRepository;

public interface CityRepository extends CrudRepository<City, Long> {
    City findByName(String name);
}
