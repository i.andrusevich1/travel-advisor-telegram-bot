package com.andrusevich.pets.traveladvisor.service;

import com.andrusevich.pets.traveladvisor.persistence.entity.City;
import com.andrusevich.pets.traveladvisor.persistence.repository.CityRepository;
import com.andrusevich.pets.traveladvisor.service.dto.CityDto;
import com.andrusevich.pets.traveladvisor.service.mapper.CityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CityService {

    @Autowired
    private CityRepository repository;

    @Autowired
    private CityMapper mapper;

    public List<CityDto> getAll(){
        List<CityDto> cities = new ArrayList<>();
        repository.findAll()
                .forEach(city -> cities.add(mapper.toDto(city)));
        return cities;
    }

    public CityDto findByName(String name){
        return mapper.toDto(repository.findByName(name));
    }

    public CityDto findById(long id){
        City city = repository.findById(id).orElseThrow(EntityNotFoundException::new);
        return mapper.toDto(city);
    }

    public CityDto create(CityDto dto){
        City city = mapper.toEntity(dto);
        return mapper.toDto(repository.save(city));
    }

    public CityDto update(CityDto dto){
        repository.findById(dto.getId()).orElseThrow(EntityNotFoundException::new);
        City city = mapper.toEntity(dto);
        return mapper.toDto(repository.save(city));
    }

    public void delete(long id){
        Optional<City> optional = repository.findById(id);
        City city = optional.orElseThrow(EntityNotFoundException::new);
        repository.delete(city);
    }
}
