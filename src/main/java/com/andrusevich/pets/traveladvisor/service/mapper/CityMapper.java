package com.andrusevich.pets.traveladvisor.service.mapper;

import com.andrusevich.pets.traveladvisor.persistence.entity.City;
import com.andrusevich.pets.traveladvisor.service.dto.CityDto;
import org.springframework.stereotype.Component;

@Component
public class CityMapper {
    public City toEntity(CityDto dto){
        return City.builder()
                .id(dto.getId())
                .name(dto.getName())
                .description(dto.getDescription())
                .build();
    }

    public CityDto toDto(City city){
        return CityDto.builder()
                .id(city.getId())
                .name(city.getName())
                .description(city.getDescription())
                .build();
    }
}
