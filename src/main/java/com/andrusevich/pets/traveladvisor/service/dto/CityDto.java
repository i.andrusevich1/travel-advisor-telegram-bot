package com.andrusevich.pets.traveladvisor.service.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Builder
public class CityDto {

    private long id;

    @Size(min = 1, max = 100)
    @Pattern(regexp = "[а-яА-ЯёЁ\\-]+")
    private String name;

    @Size(min = 1, max = 10000)
    private String description;
}
