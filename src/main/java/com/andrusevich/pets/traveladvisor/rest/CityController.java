package com.andrusevich.pets.traveladvisor.rest;

import com.andrusevich.pets.traveladvisor.service.CityService;
import com.andrusevich.pets.traveladvisor.service.dto.CityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CityController {

    @Autowired
    private CityService service;

    @GetMapping(value = "/cities")
    public ResponseEntity<List<CityDto>> getAll() {
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/cities/{id}")
    public ResponseEntity<CityDto> getById(@PathVariable("id") long id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @PostMapping(value = "/cities")
    public ResponseEntity<CityDto> create(@Valid @RequestBody CityDto input, UriComponentsBuilder uriBuilder) {
        CityDto responseBody = service.create(input);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriBuilder.path("/city/{id}").buildAndExpand(responseBody.getId()).toUri());
        return new ResponseEntity<>(responseBody, headers, HttpStatus.CREATED);
    }

    @PutMapping(value = "cities/{id}")
    public ResponseEntity<CityDto> update(@Valid @RequestBody CityDto input, @PathVariable("id") long id) {
        input.setId(id);
        return new ResponseEntity<>(service.update(input), HttpStatus.OK);
    }

    @DeleteMapping(value = "cities/{id}")
    public ResponseEntity<CityDto> delete(@PathVariable("id") long id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
