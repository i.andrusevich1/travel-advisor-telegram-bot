package com.andrusevich.pets.traveladvisor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
public class TravelAdviserApplication {

	public static void main(String[] args) {
		ApiContextInitializer.init();
		SpringApplication.run(TravelAdviserApplication.class, args);
	}

}
